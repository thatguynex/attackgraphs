var plotly = require('plotly')("thatguynex", "tepwm3PY8GkwthG7zEG3")
var fs = require('fs');

var trace1 = {
    x: ["2013-10-04 22:23:00", "2013-11-04 22:23:00", "2013-12-04 22:23:00"],
    y: [100, 31, 60],
    mode: "markers",
    name: "Chicago",
    text: ["United States", "Canada"],
    marker: {
      color: "rgb(230, 0, 0)",
      size: 12,
    },
    type: "scatter"
  };
  var data = [trace1];


var graphOptions = {filename: "date-axe", fileopt: "overwrite"};

var layout = {
    title: "Attacks",
    xaxis: {
      title: "Date",
      showgrid: false,
      zeroline: true
    },
    yaxis: {
      title: "Size (Mb)",
      showline: false
    }
  };



var figure = { 'data': data, 'graphOptions': graphOptions, 'layout': layout };

var imgOpts = {
    format: 'png',
    width: 1000,
    height: 500
};

/*plotly.plot(data, graphOptions, function (err, msg) {
    console.log(msg);
});*/

plotly.getImage(figure, imgOpts, function (error, imageStream) {
    if (error) return console.log (error);

    var fileStream = fs.createWriteStream('/mnt/h/2.png');
    imageStream.pipe(fileStream);
});