const mysql = require('mysql');
const express = require('express')
const app = express()
var plotly = require('plotly')("thatguynex", "tepwm3PY8GkwthG7zEG3")
var fs = require('fs');
var term = require( 'terminal-kit' ).terminal ;
var randomstring = require("randomstring");
var bodyParser = require('body-parser')
var geoip = require('geoip-lite');
var SqlString = require('sqlstring');
var HTMLescape = require('escape-html');

const http = require('http');
const https = require('https');

app.set('view engine', 'ejs');
app.use(express.static('public', { dotfiles: 'allow' }));
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

var isSyncing = false


//Configurator
var configurator = require('./configurator.js');
configurator.define('STAGING_HOST', '');
configurator.define('STAGING_USER', '');
configurator.define('STAGING_PASSWORD', '');
configurator.define('STAGING_DATABASE', '');
configurator.define('SQL_HOST', '');
configurator.define('SQL_USER', '');
configurator.define('SQL_PASSWORD', '');
configurator.define('SQL_DATABASE', '');
configurator.define('SQL_MAX_CON', 10);
var opt = configurator.getopts();


//------------INIT---------

var StagingSQL = mysql.createPool({
    connectionLimit : opt.SQL_MAX_CON,
    host: opt.STAGING_HOST,
    user: opt.STAGING_USER,
    password: opt.STAGING_PASSWORD,
    database: opt.STAGING_DATABASE
  });


var con = mysql.createConnection({
    host: opt.SQL_HOST,
    user: opt.SQL_USER,
    password: opt.SQL_PASSWORD,
    database: opt.SQL_DATABASE
});


var launchInit = function() {
    let currentdate = new Date();
    console.log('Caching last month from current time - ' + currentdate.addMinutes(-60 * 24 * 1).toMysqlFormat() + ' - ' + currentdate.toMysqlFormat() + '');

        let testdate = new Date().addMinutes(60);
        let hrstart = process.hrtime();
        console.log('Starting draw test...');
       //Graph.plotAllAttacks(testdate.addMinutes(-60 * 24 * 1), testdate, (filepath) => {
                    let hrend = process.hrtime(hrstart);
                    let ms = hrend[1] / 1000000;
                    console.log('Total request time: ' + hrend[0] + 's . ' + ms + 'm');
                    //console.log('Done');
                    let used = process.memoryUsage().heapUsed / 1024 / 1024;
                    console.log(`Using approximately ${Math.round(used * 100) / 100} MB of RAM`);
                    //AttackTable.SyncDatabase(() => {});
                    app.listen(4000, () => console.log(`Graphing server listening on port 4000!`))
                    //syncTimer();
                    //setInterval(syncTimer, 1000 * 60 * 10); // Ten minutes?
        //}); 
}


console.log('Preparing for initialization..');
con.connect(function(err) {
    if (err) {console.log(err);}

    if ( process.argv[2] == '--reset'){
        let testdate = new Date();//2019, 07, 21, 2, 9, 10, 0);
        let hrstart = process.hrtime();

        let onComplete = function (numProcessed) {
            if (numProcessed == 0){
                launchInit();
            } else {
                console.log('Compressed ' + numProcessed + ' rows.');
                //setTimeout(function() {
                    testdate = testdate.addMinutes(-60 * 24);
                    console.log('Starting compression for day block ' + testdate.toMysqlFormat());
                    AttackTable.CompressTimeFrame(testdate.addMinutes(-60 * 24), testdate, onComplete);
                //}, 100);
            }
        }

        AttackTable.CompressTimeFrame(testdate.addMinutes(-60 * 24), testdate, onComplete) 
    } else {launchInit()}

    //Testing

    //DRAW TEST 
    /*let testdate = new Date();
    let hrstart = process.hrtime();
    Graph.drawAllAttacks(testdate.addMinutes(-60 * 24 * 365), testdate, (filepath) => {
                let hrend = process.hrtime(hrstart);
                let ms = hrend[1] / 1000000;
                console.log('Total request time: ' + hrend[0] + 's . ' + ms + 'm');
                console.log('Done');
                let used = process.memoryUsage().heapUsed / 1024 / 1024;
                console.log(`Using approximately ${Math.round(used * 100) / 100} MB of RAM`);
                setTimeout(function() {
                    process.exit()
                }, 100);
    }); */


    //let testdate = new Date(2019, 07, 21, 2, 9, 10, 0);
    //console.log(testdate.toMysqlFormat());
    //let hrstart = process.hrtime();
    //AttackTable.RequestBlock(testdate, (block) => {
        //console.log(block);
        //console.log(AttackTable.Blocks);
        //let hrend = process.hrtime(hrstart);
        //let ms = hrend[1] / 1000000;
        //console.log('\nBlock cache / request time: ' + hrend[0] + 's | ' + ms + 'ms\n');


        /*  DOUBLE RANGE REQUEST TEST ////////
        console.log('Loading test day of entries...');
        let hrstart = process.hrtime();
        AttackTable.GetBlocksInTimeFrame(new Date(2019, 07, 20, 22, 9, 00, 0), new Date(2019, 07, 22, 0, 9, 10, 0), (blocks) => {
            let hrend = process.hrtime(hrstart);
            let ms = hrend[1] / 1000000;
            console.log('\nTimeframe request time: ' + hrend[0] + 's . ' + ms + 'm');
            console.log('Retrieved ' + blocks.length + ' blocks.')
            hrstart = process.hrtime();
            AttackTable.GetBlocksInTimeFrame(new Date(2019, 07, 20, 22, 9, 00, 0), new Date(2019, 07, 22, 0, 9, 10, 0), (blocks) => {
                let hrend = process.hrtime(hrstart);
                let ms = hrend[1] / 1000000;
                console.log('\nTimeframe request time: ' + hrend[0] + 's . ' + ms + 'm');
                console.log('Retrieved ' + blocks.length + ' blocks.')
                console.log('Done');
                process.exit()
            })
        }) */

    //})
});

var syncTimer = function() {
    if (isSyncing == false){
        isSyncing = true;
        AttackTable.SyncDatabase(() => {isSyncing = false;});
    }
}



///--------WEB SERVER ------------------
/*app.get('/', (req, res) => {
    res.render('index', {foo: 'FOO'});
});*/
app.get('/showall', (req, res) => {
    console.log('\n' + new Date().toMysqlFormat() + ': New "showall" job requested...');
    let testdate = new Date();
    Graph.plotAllAttacks(testdate.addMinutes(-60 * 24 * 31), testdate, (id, figure) => {
        res.render('rendergraph.ejs', {jdata: JSON.stringify(figure)});
        //PlotDataBuffer[id] = figure;
        //res.redirect('/viewgraph?id=' + id);
    });
});

app.get('/showattack', (req, res) => {
    console.log('\n' + new Date().toMysqlFormat() + ': New "showattack" job requested...');
    let testdate = new Date();
    if (typeof req.query.id == 'undefined'){console.log('No ID passed for single attack display!')} else {
        Graph.plotAttackGeo(req.query.id, (figure) => {
            res.render('renderattack.ejs', {jdata: JSON.stringify(figure)});
        });
    }
});

app.get('/viewip', (req, res) => {
    console.log('\n' + new Date().toMysqlFormat() + ': New "viewip" job requested...');
    if (typeof req.query.ip == 'undefined'){console.log('No IP passed for single IP graph!')} else {
        let testdate = new Date();
        Graph.plotSingleIP(req.query.ip, testdate.addMinutes(-60 * 24 * 31), testdate, (id, figure) => {
            res.render('rendergraph.ejs', {jdata: JSON.stringify(figure)});
            //PlotDataBuffer[id] = figure;
            //res.redirect('/viewgraph?id=' + id);
        });
    }
});

app.get('/attacker', (req, res) => {
    console.log('\n' + new Date().toMysqlFormat() + ': New "attacker" job requested...');
    if (typeof req.query.ip == 'undefined'){res.send('No IP passed for attacker!')} else {
        Graph.profileAttacker(req.query.ip, (figure) => {
            res.render('rendergraph.ejs', {jdata: JSON.stringify(figure)});
        });
    }
});



//-------------GRAPH GENERATION

var Graph = {
    drawAllAttacks: function(start, end, cb){
        AttackTable.GetBlocksInTimeFrame(start, end, (blocks) => {
            let figure = Graph.defaultFigure();
            console.log('Building graph definition...' + blocks.length + ' blocks.');
            //console.log(blocks);
            for(let block of blocks) {
                for(let atk of block.all) {
                    figure.data[0]['x'].push(atk.timestamp.toMysqlFormat());
                    figure.data[0]['y'].push(atk.size);
                    //console.log('Adding point ' + atk.timestamp.toMysqlFormat() + ' ' + atk.size);
                }
            }
            console.log('Building image...')
            var imgOpts = {
                format: 'png'
            };
            let hrstart = process.hrtime();
            plotly.getImage(figure, imgOpts, function (error, imageStream) {
                let hrend = process.hrtime(hrstart);
                let ms = hrend[1] / 1000000;
                console.log('Image build time: ' + hrend[0] + 's . ' + ms + 'm');
                if (error) return console.log (error);
                var fileStream = fs.createWriteStream(`./public/graphs/all_${new Date().toMysqlFormat()}.png`);
                imageStream.pipe(fileStream);
                imageStream.on('close', () => {
                    cb(fileid);
                  });
            });
        });
    },

    plotAllAttacks: function(start, end, cb){
        AttackTable.GetBlocksInTimeFrame(start, end, (blocks) => {
            let figure = Graph.defaultFigure();
            console.log('Plotting graph definition...' + blocks.length + ' blocks.');
            //console.log(blocks);
            for(let block of blocks) {
                for(let atk of block.all) {
                    figure.data[0]['x'].push(atk.timestamp.toMysqlFormat());
                    figure.data[0]['y'].push(atk.size / 1024);
                    figure.data[0]['text'].push("<h5><b>" + atk.target + '</b></h5>');
                    //console.log('Adding point ' + atk.timestamp.toMysqlFormat() + ' ' + atk.size);
                }
            }
            let id = randomstring.generate({length: 5, charset: 'hex'});
            console.log('Generated graph id ' + id);
            cb(id, figure);
        });
    },

    plotSingleIP: function(ip, start, end, cb){
        AttackTable.GetBlocksInTimeFrame(start, end, (blocks) => {
            let figure = Graph.defaultFigure();
            figure.layout.title = 'Attacks against ' + ip
            //console.log('Plotting graph definition for single IP: ' + ip);
            //console.log(blocks);
            for(let block of blocks) {
                for(let atk of block.all) {
                    //console.log('Target IP = ' + ip + '  -  Comparison =  ' + atk.target );
                    if (atk.target == ip){
                        figure.data[0]['x'].push(atk.timestamp.toMysqlFormat());
                        figure.data[0]['y'].push(atk.size / 1024);
                        figure.data[0]['text'].push("<b>" + atk.target + '</b>');
                    }
                    //console.log('Adding point ' + atk.timestamp.toMysqlFormat() + ' ' + atk.size);
                }
            }
            let id = randomstring.generate({length: 5, charset: 'hex'});
            console.log('Generated graph id ' + id);
            cb(id, figure);
        });
    },

    plotAttackGeo: function (id, cb){
        con.query(`SELECT * FROM log_attackers WHERE id = ${SqlString.escape(id)}`, function (err, result, fields) {
            console.log(`Pulled ${result.length} results.`);
            
            //Organize data
            let raw_attackers = JSON.parse(result[0].attackers);
            let raw_sizes = JSON.parse(result[0].sizes);
            let points = {}
            for (let i = 0; i < raw_attackers.length; i++){
                let geo = geoip.lookup(raw_attackers[i]);
                if (geo != null){
                    let geohash = '' + geo.ll[0] + ' ' + geo.ll[1];
                    if (typeof points[geohash] == 'undefined'){points[geohash] = {size: 0, ll: geo.ll, count: 0, loc: HTMLescape(geo.city) + ', ' + geo.country};}
                    let point = points[geohash];
                    point.size += raw_sizes[i];
                    point.count += 1;
                }
            }

            //Map data to figure
            let figure = Graph.defaultGeoFigure();
            let trace = figure.data[0];
            trace.text = [];
            trace.hovertemplate = 'Contributors: %{text}<extra></extra>';
            for (let key in points){
                let point = points[key];
                trace.lat.push(point.ll[0]);
                trace.lon.push(point.ll[1]);
                trace.marker.size.push(point.count.clamp(4,80));
                trace.marker.color.push(point.size);
                trace.text.push('' + point.count + '<br />' + point.loc + '<br />' + (point.size / 1024).toFixed(2) + " Gbps")
            }
            cb(figure);
        });
    },

    profileAttacker: function(ip, cb){
        console.log('Creating profile for attacker ip: ' + ip);
        console.log(`SELECT * FROM log WHERE ip=${SqlString.escape(ip)}`);
        StagingSQL.query(`SELECT * FROM log WHERE ip=${SqlString.escape(ip)}`, function (err, result, fields) {
            console.log(`Pulled ${result.length} results.`);
            let figure = Graph.defaultFigure();
            figure.layout.title = 'Activity From IP: ' + ip;
            figure.layout.yaxis.title = 'Traffic (Mbps)'
            figure.layout.showlegend = true;
            figure.layout.legend = {
                x: 1,
                y: 1
              }
            let event = {};
            let Attacks = figure.data[0];
            Attacks.name = 'Attacks';
            Attacks.hovertemplate = 'Target: %{text}';
            for (let i in result){
                event = result[i];
                //console.log(event); 
                Attacks['x'].push(event.timestamp.toMysqlFormat());
                Attacks['y'].push(event.size);
                Attacks['text'].push("<b>" + event.target + '</b><br />PPS: ' + event.misc + '<br /><b>' + event.timestamp.toMysqlFormat() + '</b>');
            }
            console.log(`SELECT ip, timestamp FROM log_probe WHERE ip=${SqlString.escape(ip)}`)
            StagingSQL.query(`SELECT ip, timestamp FROM log_probe WHERE ip=${SqlString.escape(ip)}`, function (err, result, fields) {
                console.log(`Pulled ${result.length} results.`)
                let Scans = Graph.defaultFigure().data[0];
                Scans.name = 'Scans';
                Scans.marker.color = "rgb(180, 80, 255)";
                Scans.hovertemplate = '%{text}';
                for (let i in result){
                    event = result[i];
                    Scans['x'].push(event.timestamp.toMysqlFormat());
                    Scans['y'].push(0);
                    Scans['text'].push(`<b>${event.timestamp.toMysqlFormat()}</b>`);
                }
                figure.data = [Attacks, Scans];
                cb(figure);
            })
        })
    },

    defaultGeoFigure: function(){
        let result = {}
        result.data = [{
            type: 'scattergeo',
            mode: 'markers',
            lat: [],
            lon:[],
            marker: {
                size: [],
                color: [],
                colorscale: 'Bluered',
                colorbar: {
                    title: 'Traffic Sent',
                    ticksuffix: ' Mbps',
                    showticksuffix: 'last'
                },
                line: {
                    color: 'black'
                }
            }
        }];
        
        result.layout = {
            'geo': {
                'scope': 'world',
                'resolution': 50
            }
        };

        return result;
    },

    defaultFigure: function(){
        let result = {}
        result.data = []
        result.data[0] = {
            x: [],
            y: [],
            text: [],
            mode: "markers",
            marker: {
              color: "rgb(255, 217, 102)",
              size: 12,
              line: {
                color: "white",
                width: 0.5
              }
            },
            type: "scatter"
        };
        result.layout = {
            xaxis: {
              title: "Date",
              showgrid: false,
              zeroline: true
            },
            yaxis: {
              title: "Size (Gb)",
              showline: false
              //yaxis: {range: [0, 200000]}
            },
            hovermode:'closest'
        };
        result.graphOptions = {filename: "date-axe", fileopt: "overwrite"};
        return result
    }
}





//-----------DATA DEFINITIONS

var AttackTable = {
    Blocks: {},

    AddEvent: function(attack){
        attack.sqldate = attack.timestamp.toMysqlFormat();
       let blockid = hash.timestamp(attack.timestamp).toMysqlFormat();
       //console.log('Adding row to blockid ' + blockid);
       if (typeof this.Blocks[blockid] == 'undefined'){this.Blocks[blockid] = new AttackBlock}
       let block = this.Blocks[blockid];
       block.all.push(attack);
       if (typeof block.ip[attack.ip] == 'undefined'){block.ip[attack.ip] = [attack]} else {block.ip[attack.ip].push(attack)}
       if (typeof block.target[attack.target] == 'undefined'){block.target[attack.target] = [attack]} else {block.target[attack.target].push(attack)}
       let sizehash = hash.size(attack.size);
       if (typeof block.size[sizehash] == 'undefined'){block.size[sizehash] = [attack]} else {block.size[sizehash].push(attack)}
    },

    GetBlocksInTimeFrame: function(start, end, cb){
        console.log('Loading blocks from ' + start.toMysqlFormat() + ' through ' + end.toMysqlFormat());
        let _start = hash.timestamp(start);
        let _end = hash.timestamp(end).addMinutes(60);
        let result = [];
        let checkedin = 0;
        let waitingon = 0;
        let iswaiting = false;

        let addToResult = function (block){
            result.push(block);
            checkedin += 1;
            //console.log('Step.. ' + checkedin);
            if (checkedin == waitingon && iswaiting) {
                //console.log('Completed sweep. ' + result.length + ' blocks loaded.');
                cb(result);
            }
        }

        //AttackTable.CacheTimeFrame(_start, _end, () => {
            //console.log('Starting walk...')
            while (_start.getTime() != _end.getTime()){
                waitingon += 1;
                //console.log('heel.. ' + waitingon);
                AttackTable.RequestBlock(_start, addToResult);
                _start = _start.addMinutes(60);
            }
            iswaiting = true;
            if (checkedin == waitingon) {
                //console.log('Completed sweep. ' + result.length + ' blocks loaded.');
                cb(result);
            }
        //});
    },

    RequestBlock: function(time, cb){
        let tbuffer = this;
        let timehash = hash.timestamp(time);
        //console.log('Requesting block ' + time.toMysqlFormat());
        if (typeof this.Blocks[timehash.toMysqlFormat()] == 'undefined'){
            this.Blocks[timehash.toMysqlFormat()] = new AttackBlock;//Create block container now to prevent repetedly trying to load empty blocks
            AttackTable.CacheTimeFrame(timehash, timehash.addMinutes(60), () => {
                term.moveTo.cyan(0,term.height,'Loaded block ' + timehash.toMysqlFormat());
                cb(tbuffer.Blocks[timehash.toMysqlFormat()]);
            });
        } else {
            cb( this.Blocks[timehash.toMysqlFormat()] );
        }
    },

    CacheTimeFrame: function(start, end, cb){
        let buffer = this;
        con.query(`SELECT * FROM log_compressed WHERE timestamp BETWEEN '${start.toMysqlFormat()}' AND '${end.toMysqlFormat()}'`, function (err, result, fields) {
            if (err) {console.log(err);} else {
                //console.log('Pulled ' + result.length + ' results.  err :' + err);
                for (var i in result) {
                    term.moveTo.green(0,term.height - 3,'Packing result ' + result[i]['id'])
                    //let atklist = new AttackerList;
                    //for (var atkkey in result.attackers){
                    //    atklist.addRow(result.attackers[atkkey], result.sizes[atkkey]);
                    //}
                    let newAttack = new AttackEvent(result[i]['id'], result[i]['timestamp'], result[i]['target'], result[i]['size']);
                    //console.log(newAttack);
                    buffer.AddEvent(newAttack);
                }
                cb();
            }
        });
    },

    SyncDatabase: function (cb){
        let currentTime = hash.timestamp(new Date());
        console.log('[Sync] Starting new sync at ' + currentTime.toMysqlFormat());
        
        let recursiveCheck = function(){
            term.moveTo.cyan(0,term.height - 1,'-----------------------------\n[Sync]Starting recursive check for block ' + currentTime.toMysqlFormat())
            if (typeof AttackTable.Blocks[currentTime.toMysqlFormat()] == 'undefined'){
                term.moveTo.cyan(0,term.height - 1,'[Sync] Block not loaded in memory, loading before continuing...');
                AttackTable.RequestBlock(currentTime,() => {recursiveCheck();});
            }else{
                if (Object.keys(AttackTable.Blocks[currentTime.toMysqlFormat()]['all']).length == 0){
                    AttackTable.Blocks[currentTime.toMysqlFormat()]['all']['Needs Sync'] = false;
                    term.moveTo.cyan(0,term.height - 1,'[Sync] Block needs sync, beginning process...');
                    AttackTable.CompressTimeFrame(currentTime, currentTime.addMinutes(60), () => {
                        term.moveTo.cyan(0,term.height - 1,'[Sync] Raw data processed, reloading block...');
                        AttackTable.RequestBlock(currentTime,() => {    
                            currentTime = currentTime.addMinutes(-60);
                            recursiveCheck();
                        });
                    });
                } else {term.moveTo.cyan(0,term.height,'[Sync] Found data, ending sync.'); cb()}
            }
        }

        //make this refresh the current block before launching recursive check, drop current block from compressed db first.
        recursiveCheck();
    },

    CompressTimeFrame: function(start, end, cb){
        let buffer = this;
        console.log('launching query - ' + `SELECT * FROM log WHERE timestamp BETWEEN '${start.toMysqlFormat()}' AND '${end.toMysqlFormat()}'`)
        StagingSQL.query(`SELECT * FROM log WHERE timestamp BETWEEN '${start.toMysqlFormat()}' AND '${end.toMysqlFormat()}'`, function (err, result, fields) {
            if (err) {console.log(err.sqlMessage);}
            console.log('Pulled ' + result.length + ' results.  err :' + err);
            if (result.length == 0){
                console.log('No results returned.');
                cb(0);
            } else {
                //console.log(result);
                let cBuffer = new ConversionBuffer();
                for (var i in result) {
                    cBuffer.addRow(result[i]);
                }
                //console.log(cBuffer);
                cBuffer.writeToDB(() => {
                    cb(result.length);
                });
            }
        });
    }
}

var ConversionBuffer = function(){
    this.events = {};
    this.attackers = {};
}
ConversionBuffer.prototype.addRow = function(row){
    term.moveTo.magenta(0,term.height - 1,'Compressing data...');
    let id = hash.eventid(row.target, row.timestamp);
    if (typeof this.events[id] == 'undefined'){
        this.events[id] = new AttackEvent(id, row.timestamp, row.target, row.size, row.misc);
        this.attackers[id] = this.events[id]['attackers'];
        this.attackers[id].addRow(row.ip, row.size, row.misc);
    } else {
        this.attackers[id].addRow(row.ip, row.size, row.misc);
        this.events[id].size += row.size;
    }
}
ConversionBuffer.prototype.writeToDB = function(cb){
    console.log('Writing conversion to database...');
    let checkedin = 0;
    let waitingon = 0;
    let iswaiting = false;
    let buffer = this;
    console.log('Starting walk....')
    
    con.beginTransaction(function(err) {
        for (var i in buffer.events) {
            if (err) {console.log(err);}
            let sql = `INSERT INTO log_compressed (id, timestamp, target, size) VALUES ('${buffer.events[i]['id']}', '${buffer.events[i]['timestamp'].toMysqlFormat()}', '${buffer.events[i]['target']}', ${buffer.events[i]['size']});`
            let jdata = buffer.attackers[i].getJson();
            let sql2 = `INSERT INTO log_attackers (id, attackers, sizes) VALUES ('${buffer.events[i]['id']}', '${jdata.attackers}', '${jdata.sizes}');`
            //console.log(sql);
            //console.log("\n" + sql2);
            waitingon += 1;

            con.query(sql, function (err, result, fields) {
                if (err) {console.log(err.sqlMessage);}
            });

            con.query(sql2, function (err, result, fields) {
                if (err) {console.log(err.sqlMessage);}
                checkedin += 1;
                if (checkedin == waitingon && iswaiting) {
                    console.log('All checked in, commiting...')
                    con.commit(function(err) {
                        if (err) {console.log(err);}
                        cb();
                    });
                }
            })
            
        }
        iswaiting = true;
        console.log('All queries launched, awaiting check ins...');
        if (checkedin == waitingon && iswaiting) {
            console.log('All checked in, commiting...')
            con.commit(function(err) {
                if (err) {console.log(err);}
                cb();
            });
        }
    });
}

var AttackBlock = function () {
    this.ip = {};
    this.target = {};
    this.size = {};
    this.all = [];
}

var AttackEvent = function(id, timestamp, target, size, attackers){
    this.id = id;
    this.timestamp = timestamp;
    this.target = target;
    this.size = size;
    //if (typeof attackers == 'undefined'){attackers = new AttackerList}
    this.attackers = new AttackerList;
}

var AttackerList = function(){
    this.data = {};
}
AttackerList.prototype.addRow = function(ip, size){
    if (typeof this.data[ip] == 'undefined'){
        this.data[ip] = {
            size: size
        }
    } else {
        this.data[ip]['size'] += size;
    }
}
AttackerList.prototype.getJson = function(){
    let result = {attackers: [], sizes: []};
    for (var i in this.data) {
        result.attackers.push(i);
        result.sizes.push(this.data[i]['size']);
        //result.packets.push(this.data[i]['packets']);
    }
    result.attackers = JSON.stringify(result.attackers);
    result.sizes = JSON.stringify(result.sizes);
    //result.packets = JSON.stringify(result.packets);
    return result
}

var ScanEvent = function(ip, timestamp){
    this.ip = ip;
    this.timestamp = timestamp;
}



var hash = {
    timestamp: function(timestamp){
        let timehash = new Date(timestamp.getTime());
        timehash.setMinutes(0, 0, 0);
        return timehash;
    },
    size: function(size){
        return Math.round(size / 1024);
    },
    eventid: function(target, timestamp){
        let id = '' + target + timestamp.getTime();
        return id.replace(" ", '');
    }
}



function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getFullYear() + "-" + twoDigits(this.getMonth() + 1) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
};
Date.prototype.addMinutes = function(mins) {
    return new Date(this.getTime() + mins*60000)
}

Number.prototype.clamp = function(min, max) {
    return Math.min(Math.max(this, min), max);
  };