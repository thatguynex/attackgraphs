var fs = require('fs');

var config = {
    define: function(option, defaultval){
        if (filebuffer === ''){
            config.loadAll()
        }
        if (typeof database[option] == 'undefined'){ 
            database[option] = defaultval 
            //console.log(option + ' set to default of ' + defaultval)
        } else if (typeof defaultval == 'number') {
            database[option] = parseInt(database[option]);
        } else if (typeof defaultval == 'string') {
            database[option] = "" + database[option];
        } else if (typeof defaultval == 'object') {
                var str = database[option]
                str = str.replace('[','');
                str = str.replace(']','');
                str = str.split(', ');
                database[option] = []
                //console.log(`Loading ${option}`)
                Object.keys(str).forEach((i) => {
                    //console.log(str[i]);
                    database[option].push(str[i]);
                });
        }
    },
    getopts: function(){
        filebuffer = ''
        return database
    },
    loadAll: function(){
        //console.log('Loading configs...')
        filebuffer = "" + fs.readFileSync(config.filepath);
        filebuffer = filebuffer.split("\n");
        Object.keys(filebuffer).forEach((i) => {
            var str = filebuffer[i];
            str = str.replace("\r",'');
            if (str.startsWith('//') == false && str != ''){
                str = str.split(' = ');
                database[str[0]] = str[1];
                //console.log(`${str[0]} = ${str[1]}`)
            }
        });
    },
    filepath: 'config.cfg'
}
module.exports = config

var filebuffer = ''
var database = {}